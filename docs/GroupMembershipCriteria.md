# GroupMembershipCriteria

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MembershipType** | **string** |  | [optional] [default to null]
**IpAddressMembershipCriteria** | [***IpAddressMembershipCriteria**](IpAddressMembershipCriteria.md) |  | [optional] [default to null]
**SearchMembershipCriteria** | [***SearchMembershipCriteria**](SearchMembershipCriteria.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


