# NsxFirewallRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**RuleId** | **string** |  | [optional] [default to null]
**SectionId** | **string** |  | [optional] [default to null]
**SectionName** | **string** |  | [optional] [default to null]
**SequenceNumber** | **int32** |  | [optional] [default to null]
**SourceAny** | **bool** |  | [optional] [default to null]
**DestinationAny** | **bool** |  | [optional] [default to null]
**ServiceAny** | **bool** |  | [optional] [default to null]
**Sources** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Destinations** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Services** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Action** | [***FirewallAction**](FirewallAction.md) |  | [optional] [default to null]
**Disabled** | **bool** |  | [optional] [default to null]
**SourceInversion** | **bool** |  | [optional] [default to null]
**DestinationInversion** | **bool** |  | [optional] [default to null]
**PortRanges** | [**[]PortRange**](PortRange.md) |  | [optional] [default to null]
**LoggingEnabled** | **bool** |  | [optional] [default to null]
**Direction** | [***FirewallDirection**](FirewallDirection.md) |  | [optional] [default to null]
**Scope** | [***ScopeEnum**](ScopeEnum.md) |  | [optional] [default to null]
**NsxManagers** | [**[]Reference**](Reference.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


