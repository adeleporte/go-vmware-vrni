# NsxvManager

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**Fqdn** | **string** |  | [optional] [default to null]
**IpAddress** | [***IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]
**PrimaryNsxManager** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Vm** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Role** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


