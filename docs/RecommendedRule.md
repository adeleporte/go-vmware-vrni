# RecommendedRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sources** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Destinations** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Protocols** | **[]string** |  | [optional] [default to null]
**PortRanges** | [**[]SimplePortRange**](SimplePortRange.md) |  | [optional] [default to null]
**Action** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


