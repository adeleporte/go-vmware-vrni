/*
 * vRealize Network Insight API Reference
 *
 * vRealize Network Insight API Reference
 *
 * API version: 1.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type PanFirewallDataSourceRequest struct {
	Ip string `json:"ip,omitempty"`
	Fqdn string `json:"fqdn,omitempty"`
	// proxy vm which should register this vcenter
	ProxyId string `json:"proxy_id"`
	Nickname string `json:"nickname"`
	Enabled bool `json:"enabled,omitempty"`
	Notes string `json:"notes,omitempty"`
	Credentials *PasswordCredentials `json:"credentials,omitempty"`
}
