# SearchMembershipCriteria

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityType** | [***AllEntityType**](AllEntityType.md) |  | [optional] [default to null]
**Filter** | **string** | As defined in search end point | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


