# IpTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TagId** | **string** |  | [optional] [default to null]
**Subnets** | **[]string** |  | [optional] [default to null]
**IpAddressRanges** | [**[]IpAddressRange**](IpAddressRange.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


