/*
 * vRealize Network Insight API Reference
 *
 * vRealize Network Insight API Reference
 *
 * API version: 1.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type SimplePortRange struct {
	Start int32 `json:"start,omitempty"`
	End int32 `json:"end,omitempty"`
}
