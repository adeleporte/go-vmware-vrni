/*
 * vRealize Network Insight API Reference
 *
 * vRealize Network Insight API Reference
 *
 * API version: 1.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type ThresholdData struct {
	Name string `json:"name,omitempty"`
	ModelKey string `json:"modelKey,omitempty"`
}

type Threshold struct {
	Data ThresholdData `json:"data,omitempty"`
}

