# VxlanLayer2Network

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**NetworkAddresses** | **[]string** |  | [optional] [default to null]
**Gateways** | **[]string** |  | [optional] [default to null]
**SegmentId** | **int32** |  | [optional] [default to null]
**Vteps** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Scope** | [***ScopeEnum**](ScopeEnum.md) |  | [optional] [default to null]
**NsxManagers** | [**[]Reference**](Reference.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


