# SnmpConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SnmpEnabled** | **bool** |  | [optional] [default to null]
**SnmpVersion** | **string** |  | [optional] [default to null]
**ConfigSnmp2c** | [***Snmp2cConfig**](SNMP2cConfig.md) |  | [optional] [default to null]
**ConfigSnmp3** | [***Snmp3Config**](SNMP3Config.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


