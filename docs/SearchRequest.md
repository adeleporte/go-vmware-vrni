# SearchRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityType** | [***AllEntityType**](AllEntityType.md) |  | [optional] [default to null]
**Filter** | **string** | query filter | [optional] [default to null]
**SortBy** | [***SortByClause**](SortByClause.md) |  | [optional] [default to null]
**Size** | **int32** |  | [optional] [default to null]
**Cursor** | **string** |  | [optional] [default to null]
**TimeRange** | [***TimeRange**](TimeRange.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


