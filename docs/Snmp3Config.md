# Snmp3Config

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** |  | [optional] [default to null]
**ContextName** | **string** |  | [optional] [default to null]
**AuthenticationType** | **string** |  | [optional] [default to null]
**AuthenticationPassword** | **string** |  | [optional] [default to null]
**PrivacyType** | **string** |  | [optional] [default to null]
**PrivacyPassword** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


