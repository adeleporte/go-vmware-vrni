# MetricSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Metric** | **string** |  | [optional] [default to null]
**DisplayName** | **string** |  | [optional] [default to null]
**Intervals** | **[]int32** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Unit** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


