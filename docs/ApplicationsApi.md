# \ApplicationsApi

All URIs are relative to *https://vrni.example.com/api/ni*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddApplication**](ApplicationsApi.md#AddApplication) | **Post** /groups/applications | Create an application
[**AddTier**](ApplicationsApi.md#AddTier) | **Post** /groups/applications/{id}/tiers | Create tier in application
[**DeleteApplication**](ApplicationsApi.md#DeleteApplication) | **Delete** /groups/applications/{id} | Delete an application
[**DeleteTier**](ApplicationsApi.md#DeleteTier) | **Delete** /groups/applications/{id}/tiers/{tier-id} | Delete tier
[**GetApplication**](ApplicationsApi.md#GetApplication) | **Get** /groups/applications/{id} | Show application details
[**GetApplicationTier**](ApplicationsApi.md#GetApplicationTier) | **Get** /groups/applications/{id}/tiers/{tier-id} | Show tier details
[**GetTier**](ApplicationsApi.md#GetTier) | **Get** /groups/tiers/{tier-id} | Show tier details
[**ListApplicationTiers**](ApplicationsApi.md#ListApplicationTiers) | **Get** /groups/applications/{id}/tiers | List tiers of an application
[**ListApplications**](ApplicationsApi.md#ListApplications) | **Get** /groups/applications | List applications
[**ListApplicationsDetails**](ApplicationsApi.md#ListApplicationsDetails) | **Get** /groups/applications/fetch | Get application details bulk


# **AddApplication**
> Application AddApplication(ctx, body)
Create an application

Application is a group of tiers. A tier is a group of virtual machines based on membership criteria. Tiers are bound to single application. An application name is unique and should not conflict with another application name.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**ApplicationRequest**](ApplicationRequest.md)|  | 

### Return type

[**Application**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddTier**
> Tier AddTier(ctx, id, body)
Create tier in application

Create a tier of an application by with specified membership criteria. The membership criteria id defined in terms of virtual machines or ip addresses/subnet. Please refer to API Guide on how to construct membership criteria.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
  **body** | [**TierRequest**](TierRequest.md)|  | 

### Return type

[**Tier**](Tier.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteApplication**
> DeleteApplication(ctx, id)
Delete an application

Deleting an application deletes all the tiers of the application along with the application

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteTier**
> DeleteTier(ctx, id, tierId)
Delete tier

Delete tier of an application

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
  **tierId** | **string**|  | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetApplication**
> Application GetApplication(ctx, id)
Show application details

Show application details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**Application**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetApplicationTier**
> Tier GetApplicationTier(ctx, id, tierId)
Show tier details

Show tier details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
  **tierId** | **string**|  | 

### Return type

[**Tier**](Tier.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetTier**
> Tier GetTier(ctx, tierId, authorization)
Show tier details

Show tier details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **tierId** | **string**|  | 
  **authorization** | **string**| Authorization Header | 

### Return type

[**Tier**](Tier.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListApplicationTiers**
> TierListResponse ListApplicationTiers(ctx, id)
List tiers of an application

List tiers of an application

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**TierListResponse**](TierListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListApplications**
> PagedListResponse ListApplications(ctx, optional)
List applications

List applications

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListApplicationsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListApplicationsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponse**](PagedListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListApplicationsDetails**
> PagedApplicationListResponse ListApplicationsDetails(ctx, optional)
Get application details bulk

Get application details bulk

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListApplicationsDetailsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListApplicationsDetailsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 

### Return type

[**PagedApplicationListResponse**](PagedApplicationListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

