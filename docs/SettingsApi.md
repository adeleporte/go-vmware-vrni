# \SettingsApi

All URIs are relative to *https://vrni.example.com/api/ni*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddIpTag**](SettingsApi.md#AddIpTag) | **Post** /settings/ip-tags/{tag-id}/add | Tag ip addresses with tag-id
[**GetIpTag**](SettingsApi.md#GetIpTag) | **Get** /settings/ip-tags/{tag-id} | Show ip tag details
[**GetIpTagIds**](SettingsApi.md#GetIpTagIds) | **Get** /settings/ip-tags/tag-ids | Show ip tag ids
[**RemoveIpTag**](SettingsApi.md#RemoveIpTag) | **Post** /settings/ip-tags/{tag-id}/remove | Remove tag from ip addresses


# **AddIpTag**
> AddIpTag(ctx, tagId, authorization, body)
Tag ip addresses with tag-id

Tag ip addresses with tag-id

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **tagId** | **string**|  | 
  **authorization** | **string**| Authorization Header | 
  **body** | [**IpTag**](IpTag.md)| Ip Tag | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetIpTag**
> IpTag GetIpTag(ctx, tagId, authorization)
Show ip tag details

Show ip tag details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **tagId** | **string**|  | 
  **authorization** | **string**| Authorization Header | 

### Return type

[**IpTag**](IpTag.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetIpTagIds**
> IpTagIdList GetIpTagIds(ctx, authorization)
Show ip tag ids

Show ip tag ids

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **authorization** | **string**| Authorization Header | 

### Return type

[**IpTagIdList**](IpTagIdList.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RemoveIpTag**
> RemoveIpTag(ctx, tagId, authorization, body)
Remove tag from ip addresses

Remove tag from ip addresses

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **tagId** | **string**|  | 
  **authorization** | **string**| Authorization Header | 
  **body** | [**IpTag**](IpTag.md)| Ip Tag | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

