# \EntitiesApi

All URIs are relative to *https://vrni.example.com/api/ni*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EntitiesFetchPost**](EntitiesApi.md#EntitiesFetchPost) | **Post** /entities/fetch | Get details of entities
[**GetCluster**](EntitiesApi.md#GetCluster) | **Get** /entities/clusters/{id} | Show cluster details
[**GetDatacenter**](EntitiesApi.md#GetDatacenter) | **Get** /entities/vc-datacenters/{id} | Show vCenter datacenter details
[**GetDatastore**](EntitiesApi.md#GetDatastore) | **Get** /entities/datastores/{id} | Show datastore details
[**GetDistributedVirtualPortgroup**](EntitiesApi.md#GetDistributedVirtualPortgroup) | **Get** /entities/distributed-virtual-portgroups/{id} | Show distributed virtual portgroup details
[**GetDistributedVirtualSwitch**](EntitiesApi.md#GetDistributedVirtualSwitch) | **Get** /entities/distributed-virtual-switches/{id} | Show distributed virtual switch details
[**GetFirewall**](EntitiesApi.md#GetFirewall) | **Get** /entities/firewalls/{id} | Show firewall details
[**GetFirewallRule**](EntitiesApi.md#GetFirewallRule) | **Get** /entities/firewall-rules/{id} | Show firewall rule details
[**GetFlow**](EntitiesApi.md#GetFlow) | **Get** /entities/flows/{id} | Show flow details
[**GetFlows**](EntitiesApi.md#GetFlows) | **Get** /entities/flows | List flows
[**GetFolder**](EntitiesApi.md#GetFolder) | **Get** /entities/folders/{id} | Show folder details
[**GetHost**](EntitiesApi.md#GetHost) | **Get** /entities/hosts/{id} | Show host details
[**GetIPSet**](EntitiesApi.md#GetIPSet) | **Get** /entities/ip-sets/{id} | Show ip set details
[**GetLayer2Network**](EntitiesApi.md#GetLayer2Network) | **Get** /entities/layer2-networks/{id} | Show layer2 network details
[**GetNSXManager**](EntitiesApi.md#GetNSXManager) | **Get** /entities/nsx-managers/{id} | Show nsx manager details
[**GetName**](EntitiesApi.md#GetName) | **Get** /entities/names/{id} | Get name of an entity
[**GetNames**](EntitiesApi.md#GetNames) | **Post** /entities/names | Get names for entities
[**GetProblemEvent**](EntitiesApi.md#GetProblemEvent) | **Get** /entities/problems/{id} | Show problem details
[**GetSecurityGroup**](EntitiesApi.md#GetSecurityGroup) | **Get** /entities/security-groups/{id} | Show security group details
[**GetSecurityTag**](EntitiesApi.md#GetSecurityTag) | **Get** /entities/security-tags/{id} | Show security tag details
[**GetService**](EntitiesApi.md#GetService) | **Get** /entities/services/{id} | Show service details
[**GetServiceGroup**](EntitiesApi.md#GetServiceGroup) | **Get** /entities/service-groups/{id} | Show service group details
[**GetVcenterManager**](EntitiesApi.md#GetVcenterManager) | **Get** /entities/vcenter-managers/{id} | Show vCenter manager details
[**GetVm**](EntitiesApi.md#GetVm) | **Get** /entities/vms/{id} | Show vm details
[**GetVmknic**](EntitiesApi.md#GetVmknic) | **Get** /entities/vmknics/{id} | Show vmknic details
[**GetVnic**](EntitiesApi.md#GetVnic) | **Get** /entities/vnics/{id} | Show vnic details
[**ListClusters**](EntitiesApi.md#ListClusters) | **Get** /entities/clusters | List clusters
[**ListDatacenters**](EntitiesApi.md#ListDatacenters) | **Get** /entities/vc-datacenters | List vCenter datacenters
[**ListDatastores**](EntitiesApi.md#ListDatastores) | **Get** /entities/datastores | List datastores
[**ListDistributedVirtualPortgroups**](EntitiesApi.md#ListDistributedVirtualPortgroups) | **Get** /entities/distributed-virtual-portgroups | List distributed virtual portgroups
[**ListDistributedVirtualSwitches**](EntitiesApi.md#ListDistributedVirtualSwitches) | **Get** /entities/distributed-virtual-switches | List distributed virtual switches
[**ListFirewallRules**](EntitiesApi.md#ListFirewallRules) | **Get** /entities/firewall-rules | List firewall rules
[**ListFirewalls**](EntitiesApi.md#ListFirewalls) | **Get** /entities/firewalls | List firewalls
[**ListFolders**](EntitiesApi.md#ListFolders) | **Get** /entities/folders | List folders
[**ListHosts**](EntitiesApi.md#ListHosts) | **Get** /entities/hosts | List hosts
[**ListIPSets**](EntitiesApi.md#ListIPSets) | **Get** /entities/ip-sets | List ip sets
[**ListLayer2Networks**](EntitiesApi.md#ListLayer2Networks) | **Get** /entities/layer2-networks | List layer2 networks
[**ListNSXManagers**](EntitiesApi.md#ListNSXManagers) | **Get** /entities/nsx-managers | List nsx managers
[**ListProblemEvents**](EntitiesApi.md#ListProblemEvents) | **Get** /entities/problems | List problems
[**ListSecurityGroups**](EntitiesApi.md#ListSecurityGroups) | **Get** /entities/security-groups | List security groups
[**ListSecurityTags**](EntitiesApi.md#ListSecurityTags) | **Get** /entities/security-tags | List security tags
[**ListServiceGroups**](EntitiesApi.md#ListServiceGroups) | **Get** /entities/service-groups | List service groups
[**ListServices**](EntitiesApi.md#ListServices) | **Get** /entities/services | List services
[**ListVcenterManagers**](EntitiesApi.md#ListVcenterManagers) | **Get** /entities/vcenter-managers | List vCenter managers
[**ListVmknics**](EntitiesApi.md#ListVmknics) | **Get** /entities/vmknics | List vmknics
[**ListVms**](EntitiesApi.md#ListVms) | **Get** /entities/vms | List vms
[**ListVnics**](EntitiesApi.md#ListVnics) | **Get** /entities/vnics | List vnics


# **EntitiesFetchPost**
> BulkFetchResponse EntitiesFetchPost(ctx, optional)
Get details of entities

Bulk fetch of entities

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***EntitiesFetchPostOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EntitiesFetchPostOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of FetchRequest**](FetchRequest.md)|  | 

### Return type

[**BulkFetchResponse**](BulkFetchResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetCluster**
> Cluster GetCluster(ctx, id, optional)
Show cluster details

Show cluster details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetClusterOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetClusterOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**Cluster**](Cluster.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetDatacenter**
> VcDatacenter GetDatacenter(ctx, id, optional)
Show vCenter datacenter details

Show vCenter datacenter details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetDatacenterOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetDatacenterOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**VcDatacenter**](VCDatacenter.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetDatastore**
> Datastore GetDatastore(ctx, id, optional)
Show datastore details

Show datastore details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetDatastoreOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetDatastoreOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**Datastore**](Datastore.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetDistributedVirtualPortgroup**
> DistributedVirtualPortgroup GetDistributedVirtualPortgroup(ctx, id, optional)
Show distributed virtual portgroup details

Show distributed virtual portgroup details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetDistributedVirtualPortgroupOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetDistributedVirtualPortgroupOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**DistributedVirtualPortgroup**](DistributedVirtualPortgroup.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetDistributedVirtualSwitch**
> DistributedVirtualSwitch GetDistributedVirtualSwitch(ctx, id, optional)
Show distributed virtual switch details

Show distributed virtual switch details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetDistributedVirtualSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetDistributedVirtualSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**DistributedVirtualSwitch**](DistributedVirtualSwitch.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFirewall**
> BaseFirewallRule GetFirewall(ctx, id, optional)
Show firewall details

Show firewall details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetFirewallOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetFirewallOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseFirewallRule**](BaseFirewallRule.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFirewallRule**
> BaseFirewallRule GetFirewallRule(ctx, id, optional)
Show firewall rule details

Show firewall rule details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetFirewallRuleOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetFirewallRuleOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseFirewallRule**](BaseFirewallRule.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFlow**
> Flow GetFlow(ctx, id, optional)
Show flow details

Show flow details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetFlowOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetFlowOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**Flow**](Flow.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFlows**
> PagedListResponseWithTime GetFlows(ctx, optional)
List flows

List flows

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***GetFlowsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetFlowsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFolder**
> Folder GetFolder(ctx, id, optional)
Show folder details

Show folder details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetFolderOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetFolderOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**Folder**](Folder.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetHost**
> Host GetHost(ctx, id, optional)
Show host details

Show host details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetHostOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetHostOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**Host**](Host.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetIPSet**
> BaseIpSet GetIPSet(ctx, id, optional)
Show ip set details

Show ip set details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetIPSetOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetIPSetOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseIpSet**](BaseIPSet.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetLayer2Network**
> BaseL2Network GetLayer2Network(ctx, id, optional)
Show layer2 network details

Show layer2 network details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetLayer2NetworkOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetLayer2NetworkOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseL2Network**](BaseL2Network.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetNSXManager**
> BaseNsxManager GetNSXManager(ctx, id, optional)
Show nsx manager details

Show nsx manager details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetNSXManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetNSXManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseNsxManager**](BaseNSXManager.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetName**
> EntityName GetName(ctx, id, optional)
Get name of an entity

Get name of an entity

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetNameOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetNameOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**EntityName**](EntityName.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetNames**
> NamesResponse GetNames(ctx, body)
Get names for entities

Get names for entities.Limit of 1000 entities in a single request.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**NamesRequest**](NamesRequest.md)| Names Request | 

### Return type

[**NamesResponse**](NamesResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetProblemEvent**
> ProblemEvent GetProblemEvent(ctx, id, optional)
Show problem details

Show problem event details.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetProblemEventOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetProblemEventOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**ProblemEvent**](ProblemEvent.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetSecurityGroup**
> BaseSecurityGroup GetSecurityGroup(ctx, id, optional)
Show security group details

Show security group details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetSecurityGroupOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetSecurityGroupOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseSecurityGroup**](BaseSecurityGroup.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetSecurityTag**
> SecurityTag GetSecurityTag(ctx, id, optional)
Show security tag details

Show security tag details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetSecurityTagOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetSecurityTagOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**SecurityTag**](SecurityTag.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetService**
> BaseService GetService(ctx, id, optional)
Show service details

Show service details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetServiceOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetServiceOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseService**](BaseService.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetServiceGroup**
> BaseServiceGroup GetServiceGroup(ctx, id, optional)
Show service group details

Show service group details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetServiceGroupOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetServiceGroupOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseServiceGroup**](BaseServiceGroup.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVcenterManager**
> VCenterManager GetVcenterManager(ctx, id, optional)
Show vCenter manager details

Show vCenter manager details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetVcenterManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetVcenterManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**VCenterManager**](VCenterManager.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVm**
> BaseVirtualMachine GetVm(ctx, id, optional)
Show vm details

Show vm details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetVmOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetVmOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseVirtualMachine**](BaseVirtualMachine.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVmknic**
> Vmknic GetVmknic(ctx, id, optional)
Show vmknic details

Show vmknic details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetVmknicOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetVmknicOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**Vmknic**](Vmknic.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVnic**
> BaseVnic GetVnic(ctx, id, optional)
Show vnic details

Show vnic details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***GetVnicOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetVnicOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **time** | **optional.Int64**| time in epoch seconds | 

### Return type

[**BaseVnic**](BaseVnic.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListClusters**
> PagedListResponseWithTime ListClusters(ctx, optional)
List clusters

List clusters

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListClustersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListClustersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListDatacenters**
> PagedListResponseWithTime ListDatacenters(ctx, optional)
List vCenter datacenters

List vCenter datacenters

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListDatacentersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListDatacentersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListDatastores**
> PagedListResponseWithTime ListDatastores(ctx, optional)
List datastores

List datastores

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListDatastoresOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListDatastoresOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListDistributedVirtualPortgroups**
> PagedListResponseWithTime ListDistributedVirtualPortgroups(ctx, optional)
List distributed virtual portgroups

List distributed virtual portgroups

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListDistributedVirtualPortgroupsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListDistributedVirtualPortgroupsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListDistributedVirtualSwitches**
> PagedListResponseWithTime ListDistributedVirtualSwitches(ctx, optional)
List distributed virtual switches

List distributed virtual switches

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListDistributedVirtualSwitchesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListDistributedVirtualSwitchesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListFirewallRules**
> PagedListResponseWithTime ListFirewallRules(ctx, optional)
List firewall rules

List firewall rules

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListFirewallRulesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListFirewallRulesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListFirewalls**
> PagedListResponseWithTime ListFirewalls(ctx, optional)
List firewalls

List firewalls

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListFirewallsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListFirewallsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListFolders**
> PagedListResponseWithTime ListFolders(ctx, optional)
List folders

List folders

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListFoldersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListFoldersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListHosts**
> PagedListResponseWithTime ListHosts(ctx, optional)
List hosts

List hosts

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListHostsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListHostsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListIPSets**
> PagedListResponseWithTime ListIPSets(ctx, optional)
List ip sets

List ip sets

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListIPSetsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListIPSetsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListLayer2Networks**
> PagedListResponseWithTime ListLayer2Networks(ctx, optional)
List layer2 networks

List layer2 networks

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListLayer2NetworksOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListLayer2NetworksOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListNSXManagers**
> PagedListResponseWithTime ListNSXManagers(ctx, optional)
List nsx managers

List nsx managers

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListNSXManagersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListNSXManagersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListProblemEvents**
> PagedListResponseWithTime ListProblemEvents(ctx, optional)
List problems

List problem events.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListProblemEventsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListProblemEventsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListSecurityGroups**
> PagedListResponseWithTime ListSecurityGroups(ctx, optional)
List security groups

List security groups

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListSecurityGroupsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListSecurityGroupsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListSecurityTags**
> PagedListResponseWithTime ListSecurityTags(ctx, optional)
List security tags

List security tags

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListSecurityTagsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListSecurityTagsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListServiceGroups**
> PagedListResponseWithTime ListServiceGroups(ctx, optional)
List service groups

List service groups

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListServiceGroupsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListServiceGroupsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListServices**
> PagedListResponseWithTime ListServices(ctx, optional)
List services

List services

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListServicesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListServicesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListVcenterManagers**
> PagedListResponseWithTime ListVcenterManagers(ctx, optional)
List vCenter managers

List vCenter managers

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListVcenterManagersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListVcenterManagersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListVmknics**
> PagedListResponseWithTime ListVmknics(ctx, optional)
List vmknics

List vmknics

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListVmknicsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListVmknicsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListVms**
> PagedListResponseWithTime ListVms(ctx, optional)
List vms

List vms

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListVmsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListVmsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListVnics**
> PagedListResponseWithTime ListVnics(ctx, optional)
List vnics

List vnics

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ListVnicsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListVnicsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **optional.Float32**| page size of results | [default to 10]
 **cursor** | **optional.String**| cursor from previous response | 
 **startTime** | **optional.Float32**| start time for query in epoch seconds | 
 **endTime** | **optional.Float32**| end time for query in epoch seconds | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

