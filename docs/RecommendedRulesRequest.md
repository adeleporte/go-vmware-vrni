# RecommendedRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Group1** | [***MicroSecGroup**](MicroSecGroup.md) |  | [optional] [default to null]
**Group2** | [***MicroSecGroup**](MicroSecGroup.md) |  | [optional] [default to null]
**TimeRange** | [***TimeRange**](TimeRange.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


