# DellSwitchDataSource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**EntityType** | [***DataSourceType**](DataSourceType.md) |  | [optional] [default to null]
**Ip** | **string** |  | [optional] [default to null]
**Fqdn** | **string** |  | [optional] [default to null]
**ProxyId** | **string** | proxy vm which should register this vcenter | [optional] [default to null]
**Nickname** | **string** |  | [optional] [default to null]
**Enabled** | **bool** |  | [optional] [default to null]
**Notes** | **string** |  | [optional] [default to null]
**Credentials** | [***PasswordCredentials**](PasswordCredentials.md) |  | [optional] [default to null]
**SwitchType** | [***DellSwitchType**](DellSwitchType.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


