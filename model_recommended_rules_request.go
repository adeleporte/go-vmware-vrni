/*
 * vRealize Network Insight API Reference
 *
 * vRealize Network Insight API Reference
 *
 * API version: 1.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type RecommendedRulesRequest struct {
	Group1 *MicroSecGroup `json:"group_1,omitempty"`
	Group2 *MicroSecGroup `json:"group_2,omitempty"`
	TimeRange *TimeRange `json:"time_range,omitempty"`
}
