# Vpc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**CidrBlock** | [***IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**State** | **string** |  | [optional] [default to null]
**Region** | **string** |  | [optional] [default to null]
**DefaultVpc** | **bool** |  | [optional] [default to null]
**VendorId** | **string** |  | [optional] [default to null]
**LastSynchedTime** | **int64** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


