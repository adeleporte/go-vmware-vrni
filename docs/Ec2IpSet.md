# Ec2IpSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**IpAddresses** | [**[]IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**IpRanges** | [**[]IpAddressRange**](IpAddressRange.md) |  | [optional] [default to null]
**IpNumericRanges** | [**[]IpNumericRange**](IpNumericRange.md) |  | [optional] [default to null]
**ParentSecurityGroups** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**DirectSourceRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**DirectDestinationRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**IndirectSourceRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**IndirectDestinationRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**VendorId** | **string** |  | [optional] [default to null]
**Vendor** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


