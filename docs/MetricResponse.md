# MetricResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Metric** | **string** |  | [optional] [default to null]
**DisplayName** | **string** |  | [optional] [default to null]
**Interval** | **int32** |  | [optional] [default to null]
**Unit** | **string** |  | [optional] [default to null]
**Pointlist** | [**[][]float64**](array.md) |  | [optional] [default to null]
**Start** | **int64** |  | [optional] [default to null]
**End** | **int64** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


