# IpV4Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IpAddress** | **string** |  | [optional] [default to null]
**Netmask** | **string** |  | [optional] [default to null]
**NetworkAddress** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


