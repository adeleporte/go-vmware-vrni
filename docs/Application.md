# Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**CreateTime** | **int64** |  | [optional] [default to null]
**CreatedBy** | **string** |  | [optional] [default to null]
**LastModifiedTime** | **int64** |  | [optional] [default to null]
**LastModifiedBy** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


