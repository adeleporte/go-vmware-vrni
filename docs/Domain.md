# Domain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DomainType** | **string** |  | [optional] [default to null]
**Value** | **string** | domain value, not required for LOCAL domain | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


