# Node

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**EntityType** | [***NodeType**](NodeType.md) |  | [optional] [default to null]
**NodeType** | **string** |  | [optional] [default to null]
**NodeId** | **string** |  | [optional] [default to null]
**IpAddress** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


