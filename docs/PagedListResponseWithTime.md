# PagedListResponseWithTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]EntityIdWithTime**](EntityIdWithTime.md) |  | [optional] [default to null]
**Cursor** | **string** |  | [optional] [default to null]
**TotalCount** | **int32** |  | [optional] [default to null]
**StartTime** | **int64** |  | [optional] [default to null]
**EndTime** | **int64** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


