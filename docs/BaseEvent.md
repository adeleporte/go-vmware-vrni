# BaseEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**AnchorEntities** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**RelatedEntities** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Message** | **string** |  | [optional] [default to null]
**EventTags** | **[]string** |  | [optional] [default to null]
**AdminState** | **string** |  | [optional] [default to null]
**Archived** | **bool** |  | [optional] [default to null]
**EventTimeEpochMs** | **int64** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


