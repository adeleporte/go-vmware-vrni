# \SearchApi

All URIs are relative to *https://vrni.example.com/api/ni*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SearchEntities**](SearchApi.md#SearchEntities) | **Post** /search | Search entities


# **SearchEntities**
> PagedListResponseWithTime SearchEntities(ctx, optional)
Search entities

Using search API you can search vRealize Network Insight entities by specifying entity type and filter expression. A filter expression is a predicate expression (similar to SQL where clause) used to define the search criteria. Please refer to API Guide on details of how to construct filter expression. A successful search request will return a list of entity ids that matches the search criteria.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***SearchEntitiesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a SearchEntitiesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of SearchRequest**](SearchRequest.md)| Search Request | 

### Return type

[**PagedListResponseWithTime**](PagedListResponseWithTime.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

