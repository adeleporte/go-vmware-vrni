# NsxServiceGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**Members** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Scope** | [***ScopeEnum**](ScopeEnum.md) |  | [optional] [default to null]
**NsxManagers** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**VendorId** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


