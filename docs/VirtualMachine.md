# VirtualMachine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**IpAddresses** | [**[]IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**DefaultGateway** | **string** |  | [optional] [default to null]
**Vnics** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SecurityGroups** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SourceFirewallRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**DestinationFirewallRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**IpSets** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Cluster** | [***Reference**](Reference.md) |  | [optional] [default to null]
**ResourcePool** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SecurityTags** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Layer2Networks** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Host** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Vlans** | [**[]Vlan**](Vlan.md) |  | [optional] [default to null]
**VendorId** | **string** |  | [optional] [default to null]
**VcenterManager** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Folders** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Datastores** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Datacenter** | [***Reference**](Reference.md) |  | [optional] [default to null]
**NsxManager** | [***Reference**](Reference.md) |  | [optional] [default to null]
**AppliedToSourceRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**AppliedToDestinationRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**SourceInversionRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**DestinationInversionRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


