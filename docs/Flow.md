# Flow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**SourceVm** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationVm** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SourceVnic** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationVnic** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SourceVpc** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationVpc** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SourceDatacenter** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationDatacenter** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SourceIp** | [***IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**DestinationIp** | [***IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**SourceL2Network** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationL2Network** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Port** | [***PortRange**](PortRange.md) |  | [optional] [default to null]
**SourceFolders** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**DestinationFolders** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SourceResourcePool** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationResourcePool** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SourceCluster** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationCluster** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Protocol** | [***Protocol**](Protocol.md) |  | [optional] [default to null]
**SourceIpSets** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**DestinationIpSets** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SourceSecurityGroups** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**DestinationSecurityGroups** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**TrafficType** | [***FlowTrafficType**](FlowTrafficType.md) |  | [optional] [default to null]
**SourceSecurityTags** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**DestinationSecurityTags** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SourceHost** | [***Reference**](Reference.md) |  | [optional] [default to null]
**DestinationHost** | [***Reference**](Reference.md) |  | [optional] [default to null]
**SourceVmTags** | **[]string** |  | [optional] [default to null]
**DestinationVmTags** | **[]string** |  | [optional] [default to null]
**WithinHost** | **bool** |  | [optional] [default to null]
**FirewallAction** | [***FirewallAction**](FirewallAction.md) |  | [optional] [default to null]
**FlowTag** | [**[]FlowTag**](FlowTag.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


