# RecommendedRules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]RecommendedRule**](RecommendedRule.md) |  | [optional] [default to null]
**TimeRange** | [***TimeRange**](TimeRange.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


