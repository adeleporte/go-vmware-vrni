# RuleSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Rules** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Firewall** | [***Reference**](Reference.md) |  | [optional] [default to null]
**RuleSetType** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


