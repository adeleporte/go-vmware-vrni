/*
 * vRealize Network Insight API Reference
 *
 * vRealize Network Insight API Reference
 *
 * API version: 1.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type SearchRequest struct {
	EntityType *AllEntityType `json:"entity_type,omitempty"`
	// query filter
	Filter string `json:"filter,omitempty"`
	SortBy *SortByClause `json:"sort_by,omitempty"`
	Size int32 `json:"size,omitempty"`
	Cursor string `json:"cursor,omitempty"`
	TimeRange *TimeRange `json:"time_range,omitempty"`
}
