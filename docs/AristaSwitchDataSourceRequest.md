# AristaSwitchDataSourceRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Ip** | **string** |  | [optional] [default to null]
**Fqdn** | **string** |  | [optional] [default to null]
**ProxyId** | **string** | proxy vm which should register this vcenter | [default to null]
**Nickname** | **string** |  | [default to null]
**Enabled** | **bool** |  | [optional] [default to null]
**Notes** | **string** |  | [optional] [default to null]
**Credentials** | [***PasswordCredentials**](PasswordCredentials.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


