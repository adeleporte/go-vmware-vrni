# Snmp2cConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CommunityString** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


