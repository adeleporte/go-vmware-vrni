# \DataSourcesApi

All URIs are relative to *https://vrni.example.com/api/ni*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddAristaSwitch**](DataSourcesApi.md#AddAristaSwitch) | **Post** /data-sources/arista-switches | Create an arista switch data source
[**AddBrocadeSwitch**](DataSourcesApi.md#AddBrocadeSwitch) | **Post** /data-sources/brocade-switches | Create a brocade switch data source
[**AddCheckpointFirewall**](DataSourcesApi.md#AddCheckpointFirewall) | **Post** /data-sources/checkpoint-firewalls | Create a checkpoint firewall
[**AddCiscoSwitch**](DataSourcesApi.md#AddCiscoSwitch) | **Post** /data-sources/cisco-switches | Create a cisco switch data source
[**AddDellSwitch**](DataSourcesApi.md#AddDellSwitch) | **Post** /data-sources/dell-switches | Create a dell switch data source
[**AddHpovManager**](DataSourcesApi.md#AddHpovManager) | **Post** /data-sources/hpov-managers | Create a hp oneview manager data source
[**AddHpvcManager**](DataSourcesApi.md#AddHpvcManager) | **Post** /data-sources/hpvc-managers | Create a hpvc manager data source
[**AddJuniperSwitch**](DataSourcesApi.md#AddJuniperSwitch) | **Post** /data-sources/juniper-switches | Add a juniper switch as data source
[**AddNsxtManagerDatasource**](DataSourcesApi.md#AddNsxtManagerDatasource) | **Post** /data-sources/nsxt-managers | Create an nsx-t manager data source
[**AddNsxvManagerDatasource**](DataSourcesApi.md#AddNsxvManagerDatasource) | **Post** /data-sources/nsxv-managers | Create an nsx-v manager data source
[**AddPanoramaFirewall**](DataSourcesApi.md#AddPanoramaFirewall) | **Post** /data-sources/panorama-firewalls | Create panorama firewall data source
[**AddUcsManager**](DataSourcesApi.md#AddUcsManager) | **Post** /data-sources/ucs-managers | Create a ucs manager data source
[**AddVcenterDatasource**](DataSourcesApi.md#AddVcenterDatasource) | **Post** /data-sources/vcenters | Create a vCenter data source
[**DeleteAristaSwitch**](DataSourcesApi.md#DeleteAristaSwitch) | **Delete** /data-sources/arista-switches/{id} | Delete an arista switch data source
[**DeleteBrocadeSwitch**](DataSourcesApi.md#DeleteBrocadeSwitch) | **Delete** /data-sources/brocade-switches/{id} | Delete a brocade switch data source
[**DeleteCheckpointFirewall**](DataSourcesApi.md#DeleteCheckpointFirewall) | **Delete** /data-sources/checkpoint-firewalls/{id} | Delete a checkpoint firewall data source
[**DeleteCiscoSwitch**](DataSourcesApi.md#DeleteCiscoSwitch) | **Delete** /data-sources/cisco-switches/{id} | Delete a cisco switch data source
[**DeleteDellSwitch**](DataSourcesApi.md#DeleteDellSwitch) | **Delete** /data-sources/dell-switches/{id} | Delete a dell switch data source
[**DeleteHpovManager**](DataSourcesApi.md#DeleteHpovManager) | **Delete** /data-sources/hpov-managers/{id} | Delete a hp oneview data source
[**DeleteHpvcManager**](DataSourcesApi.md#DeleteHpvcManager) | **Delete** /data-sources/hpvc-managers/{id} | Delete a hpvc manager data source
[**DeleteJuniperSwitch**](DataSourcesApi.md#DeleteJuniperSwitch) | **Delete** /data-sources/juniper-switches/{id} | Delete a juniper switch data source
[**DeleteNsxtManager**](DataSourcesApi.md#DeleteNsxtManager) | **Delete** /data-sources/nsxt-managers/{id} | Delete an nsx-t manager data source
[**DeleteNsxvManager**](DataSourcesApi.md#DeleteNsxvManager) | **Delete** /data-sources/nsxv-managers/{id} | Delete an nsx-v manager data source
[**DeletePanoramaFirewall**](DataSourcesApi.md#DeletePanoramaFirewall) | **Delete** /data-sources/panorama-firewalls/{id} | Delete a panorama firewall data source
[**DeleteUcsManager**](DataSourcesApi.md#DeleteUcsManager) | **Delete** /data-sources/ucs-managers/{id} | Delete a ucs manager data source
[**DeleteVcenter**](DataSourcesApi.md#DeleteVcenter) | **Delete** /data-sources/vcenters/{id} | Delete a vCenter data source
[**DisableAristaSwitch**](DataSourcesApi.md#DisableAristaSwitch) | **Post** /data-sources/arista-switches/{id}/disable | Disable an arista switch data source
[**DisableBrocadeSwitch**](DataSourcesApi.md#DisableBrocadeSwitch) | **Post** /data-sources/brocade-switches/{id}/disable | Disable a brocade switch data source
[**DisableCheckpointFirewall**](DataSourcesApi.md#DisableCheckpointFirewall) | **Post** /data-sources/checkpoint-firewalls/{id}/disable | Disable a checkpoint firewall data source
[**DisableCiscoSwitch**](DataSourcesApi.md#DisableCiscoSwitch) | **Post** /data-sources/cisco-switches/{id}/disable | Disable a cisco switch data source
[**DisableDellSwitch**](DataSourcesApi.md#DisableDellSwitch) | **Post** /data-sources/dell-switches/{id}/disable | Disable a dell switch data source
[**DisableHpovManager**](DataSourcesApi.md#DisableHpovManager) | **Post** /data-sources/hpov-managers/{id}/disable | Disable a hp oneview data source
[**DisableHpvcManager**](DataSourcesApi.md#DisableHpvcManager) | **Post** /data-sources/hpvc-managers/{id}/disable | Disable a hpvc manager data source
[**DisableJuniperSwitch**](DataSourcesApi.md#DisableJuniperSwitch) | **Post** /data-sources/juniper-switches/{id}/disable | Disable a juniper switch data source
[**DisableNsxtManager**](DataSourcesApi.md#DisableNsxtManager) | **Post** /data-sources/nsxt-managers/{id}/disable | Disable an nsx-t manager data source
[**DisableNsxvManager**](DataSourcesApi.md#DisableNsxvManager) | **Post** /data-sources/nsxv-managers/{id}/disable | Disable an nsx-v manager data source
[**DisablePanoramaFirewall**](DataSourcesApi.md#DisablePanoramaFirewall) | **Post** /data-sources/panorama-firewalls/{id}/disable | Disable a panorama firewall data source
[**DisableUcsManager**](DataSourcesApi.md#DisableUcsManager) | **Post** /data-sources/ucs-managers/{id}/disable | Disable a ucs manager data source
[**DisableVcenter**](DataSourcesApi.md#DisableVcenter) | **Post** /data-sources/vcenters/{id}/disable | Disable a vCenter data source
[**EnableAristaSwitch**](DataSourcesApi.md#EnableAristaSwitch) | **Post** /data-sources/arista-switches/{id}/enable | Enable an arista switch data source
[**EnableBrocadeSwitch**](DataSourcesApi.md#EnableBrocadeSwitch) | **Post** /data-sources/brocade-switches/{id}/enable | Enable a brocade switch data source
[**EnableCheckpointFirewall**](DataSourcesApi.md#EnableCheckpointFirewall) | **Post** /data-sources/checkpoint-firewalls/{id}/enable | Enable a checkpoint firewall data source
[**EnableCiscoSwitch**](DataSourcesApi.md#EnableCiscoSwitch) | **Post** /data-sources/cisco-switches/{id}/enable | Enable a cisco switch data source
[**EnableDellSwitch**](DataSourcesApi.md#EnableDellSwitch) | **Post** /data-sources/dell-switches/{id}/enable | Enable a dell switch data source
[**EnableHpovManager**](DataSourcesApi.md#EnableHpovManager) | **Post** /data-sources/hpov-managers/{id}/enable | Enable a hp oneview data source
[**EnableHpvcManager**](DataSourcesApi.md#EnableHpvcManager) | **Post** /data-sources/hpvc-managers/{id}/enable | Enable a hpvc manager data source
[**EnableJuniperSwitch**](DataSourcesApi.md#EnableJuniperSwitch) | **Post** /data-sources/juniper-switches/{id}/enable | Enable a juniper switch data source
[**EnableNsxtManager**](DataSourcesApi.md#EnableNsxtManager) | **Post** /data-sources/nsxt-managers/{id}/enable | Enable an nsx-t manager data source
[**EnableNsxvManager**](DataSourcesApi.md#EnableNsxvManager) | **Post** /data-sources/nsxv-managers/{id}/enable | Enable an nsx-v manager data source
[**EnablePanoramaFirewall**](DataSourcesApi.md#EnablePanoramaFirewall) | **Post** /data-sources/panorama-firewalls/{id}/enable | Enable a panorama firewall data source
[**EnableUcsManager**](DataSourcesApi.md#EnableUcsManager) | **Post** /data-sources/ucs-managers/{id}/enable | Enable a ucs manager data source
[**EnableVcenter**](DataSourcesApi.md#EnableVcenter) | **Post** /data-sources/vcenters/{id}/enable | Enable a vCenter data source
[**GetAristaSwitch**](DataSourcesApi.md#GetAristaSwitch) | **Get** /data-sources/arista-switches/{id} | Show arista switch data source details
[**GetAristaSwitchSnmpConfig**](DataSourcesApi.md#GetAristaSwitchSnmpConfig) | **Get** /data-sources/arista-switches/{id}/snmp-config | Show snmp config for arista switch data source
[**GetBrocadeSwitch**](DataSourcesApi.md#GetBrocadeSwitch) | **Get** /data-sources/brocade-switches/{id} | Show brocade switch data source details
[**GetBrocadeSwitchSnmpConfig**](DataSourcesApi.md#GetBrocadeSwitchSnmpConfig) | **Get** /data-sources/brocade-switches/{id}/snmp-config | Show snmp config for brocade switch data source
[**GetCheckpointFirewall**](DataSourcesApi.md#GetCheckpointFirewall) | **Get** /data-sources/checkpoint-firewalls/{id} | Show checkpoint firewall data source details
[**GetCiscoSwitch**](DataSourcesApi.md#GetCiscoSwitch) | **Get** /data-sources/cisco-switches/{id} | Show cisco switch data source details
[**GetCiscoSwitchSnmpConfig**](DataSourcesApi.md#GetCiscoSwitchSnmpConfig) | **Get** /data-sources/cisco-switches/{id}/snmp-config | Show snmp config for cisco switch data source
[**GetDellSwitch**](DataSourcesApi.md#GetDellSwitch) | **Get** /data-sources/dell-switches/{id} | Show dell switch data source details
[**GetDellSwitchSnmpConfig**](DataSourcesApi.md#GetDellSwitchSnmpConfig) | **Get** /data-sources/dell-switches/{id}/snmp-config | Show snmp config for dell switch data source
[**GetHpovManager**](DataSourcesApi.md#GetHpovManager) | **Get** /data-sources/hpov-managers/{id} | Show hp oneview data source details
[**GetHpvcManager**](DataSourcesApi.md#GetHpvcManager) | **Get** /data-sources/hpvc-managers/{id} | Show hpvc data source details
[**GetJuniperSwitch**](DataSourcesApi.md#GetJuniperSwitch) | **Get** /data-sources/juniper-switches/{id} | Show juniper switch data source details
[**GetJuniperSwitchSnmpConfig**](DataSourcesApi.md#GetJuniperSwitchSnmpConfig) | **Get** /data-sources/juniper-switches/{id}/snmp-config | Show snmp config for juniper switch data source
[**GetNsxtManager**](DataSourcesApi.md#GetNsxtManager) | **Get** /data-sources/nsxt-managers/{id} | Show nsx-t manager data source details
[**GetNsxvControllerCluster**](DataSourcesApi.md#GetNsxvControllerCluster) | **Get** /data-sources/nsxv-managers/{id}/controller-cluster | Show nsx controller-cluster details
[**GetNsxvManager**](DataSourcesApi.md#GetNsxvManager) | **Get** /data-sources/nsxv-managers/{id} | Show nsx-v manager data source details
[**GetPanoramaFirewall**](DataSourcesApi.md#GetPanoramaFirewall) | **Get** /data-sources/panorama-firewalls/{id} | Show panorama firewall data source details
[**GetUcsManager**](DataSourcesApi.md#GetUcsManager) | **Get** /data-sources/ucs-managers/{id} | Show ucs manager data source details
[**GetUcsSnmpConfig**](DataSourcesApi.md#GetUcsSnmpConfig) | **Get** /data-sources/ucs-managers/{id}/snmp-config | Show snmp config for ucs fabric interconnects
[**GetVcenter**](DataSourcesApi.md#GetVcenter) | **Get** /data-sources/vcenters/{id} | Show vCenter data source details
[**ListAristaSwitches**](DataSourcesApi.md#ListAristaSwitches) | **Get** /data-sources/arista-switches | List arista switch data sources
[**ListBrocadeSwitches**](DataSourcesApi.md#ListBrocadeSwitches) | **Get** /data-sources/brocade-switches | List brocade switch data sources
[**ListCheckpointFirewalls**](DataSourcesApi.md#ListCheckpointFirewalls) | **Get** /data-sources/checkpoint-firewalls | List checkpoint firewall data sources
[**ListCiscoSwitches**](DataSourcesApi.md#ListCiscoSwitches) | **Get** /data-sources/cisco-switches | List cisco switch data sources
[**ListDellSwitches**](DataSourcesApi.md#ListDellSwitches) | **Get** /data-sources/dell-switches | List dell switch data sources
[**ListHpovManagers**](DataSourcesApi.md#ListHpovManagers) | **Get** /data-sources/hpov-managers | List hp oneview manager data sources
[**ListHpvcManagers**](DataSourcesApi.md#ListHpvcManagers) | **Get** /data-sources/hpvc-managers | List hpvc manager data sources
[**ListJuniperSwitches**](DataSourcesApi.md#ListJuniperSwitches) | **Get** /data-sources/juniper-switches | List juniper switch data sources
[**ListNsxtManagers**](DataSourcesApi.md#ListNsxtManagers) | **Get** /data-sources/nsxt-managers | List nsx-t manager data sources
[**ListNsxvManagers**](DataSourcesApi.md#ListNsxvManagers) | **Get** /data-sources/nsxv-managers | List nsx-v manager data sources
[**ListPanoramaFirewalls**](DataSourcesApi.md#ListPanoramaFirewalls) | **Get** /data-sources/panorama-firewalls | List panorama firewall data sources
[**ListUcsManagers**](DataSourcesApi.md#ListUcsManagers) | **Get** /data-sources/ucs-managers | List ucs manager data sources
[**ListVcenters**](DataSourcesApi.md#ListVcenters) | **Get** /data-sources/vcenters | List vCenter data sources
[**UpdateAristaSwitch**](DataSourcesApi.md#UpdateAristaSwitch) | **Put** /data-sources/arista-switches/{id} | Update an arista switch data source
[**UpdateAristaSwitchSnmpConfig**](DataSourcesApi.md#UpdateAristaSwitchSnmpConfig) | **Put** /data-sources/arista-switches/{id}/snmp-config | Update snmp config for arista switch data source
[**UpdateBrocadeSwitch**](DataSourcesApi.md#UpdateBrocadeSwitch) | **Put** /data-sources/brocade-switches/{id} | Update a brocade switch data source
[**UpdateBrocadeSwitchSnmpConfig**](DataSourcesApi.md#UpdateBrocadeSwitchSnmpConfig) | **Put** /data-sources/brocade-switches/{id}/snmp-config | Update snmp config for brocade switch data source
[**UpdateCheckpointFirewall**](DataSourcesApi.md#UpdateCheckpointFirewall) | **Put** /data-sources/checkpoint-firewalls/{id} | Update a checkpoint firewall data source
[**UpdateCiscoSwitch**](DataSourcesApi.md#UpdateCiscoSwitch) | **Put** /data-sources/cisco-switches/{id} | Update a cisco switch data source
[**UpdateCiscoSwitchSnmpConfig**](DataSourcesApi.md#UpdateCiscoSwitchSnmpConfig) | **Put** /data-sources/cisco-switches/{id}/snmp-config | Update snmp config for cisco switch data source
[**UpdateDellSwitch**](DataSourcesApi.md#UpdateDellSwitch) | **Put** /data-sources/dell-switches/{id} | Update a dell switch data source
[**UpdateDellSwitchSnmpConfig**](DataSourcesApi.md#UpdateDellSwitchSnmpConfig) | **Put** /data-sources/dell-switches/{id}/snmp-config | Update snmp config for dell switch data source
[**UpdateHpovManager**](DataSourcesApi.md#UpdateHpovManager) | **Put** /data-sources/hpov-managers/{id} | Update a hp oneview data source
[**UpdateHpvcManager**](DataSourcesApi.md#UpdateHpvcManager) | **Put** /data-sources/hpvc-managers/{id} | Update a hpvc manager data source
[**UpdateJuniperSwitch**](DataSourcesApi.md#UpdateJuniperSwitch) | **Put** /data-sources/juniper-switches/{id} | Update a juniper switch data source
[**UpdateJuniperSwitchSnmpConfig**](DataSourcesApi.md#UpdateJuniperSwitchSnmpConfig) | **Put** /data-sources/juniper-switches/{id}/snmp-config | Update snmp config for a juniper switch data source
[**UpdateNsxtManager**](DataSourcesApi.md#UpdateNsxtManager) | **Put** /data-sources/nsxt-managers/{id} | Update an nsx-t manager data source
[**UpdateNsxvControllerCluster**](DataSourcesApi.md#UpdateNsxvControllerCluster) | **Put** /data-sources/nsxv-managers/{id}/controller-cluster | Update nsx controller-cluster details
[**UpdateNsxvManager**](DataSourcesApi.md#UpdateNsxvManager) | **Put** /data-sources/nsxv-managers/{id} | Update an nsx-v manager data source
[**UpdatePanoramaFirewall**](DataSourcesApi.md#UpdatePanoramaFirewall) | **Put** /data-sources/panorama-firewalls/{id} | Update a panorama firewall data source
[**UpdateUcsManager**](DataSourcesApi.md#UpdateUcsManager) | **Put** /data-sources/ucs-managers/{id} | Update a ucs manager data source
[**UpdateUcsSnmpConfig**](DataSourcesApi.md#UpdateUcsSnmpConfig) | **Put** /data-sources/ucs-managers/{id}/snmp-config | Update snmp config for ucs fabric interconnects
[**UpdateVcenter**](DataSourcesApi.md#UpdateVcenter) | **Put** /data-sources/vcenters/{id} | Update a vCenter data source.


# **AddAristaSwitch**
> AristaSwitchDataSource AddAristaSwitch(ctx, optional)
Create an arista switch data source

Add arista switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddAristaSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddAristaSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of AristaSwitchDataSourceRequest**](AristaSwitchDataSourceRequest.md)| Add a cisco switch as datasource | 

### Return type

[**AristaSwitchDataSource**](AristaSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddBrocadeSwitch**
> BrocadeSwitchDataSource AddBrocadeSwitch(ctx, optional)
Create a brocade switch data source

Add brocade switch as a data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddBrocadeSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddBrocadeSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of BrocadeSwitchDataSourceRequest**](BrocadeSwitchDataSourceRequest.md)|  | 

### Return type

[**BrocadeSwitchDataSource**](BrocadeSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddCheckpointFirewall**
> CheckpointFirewallDataSource AddCheckpointFirewall(ctx, optional)
Create a checkpoint firewall

Add checkpoint firewall as data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddCheckpointFirewallOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddCheckpointFirewallOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of CheckpointFirewallDataSourceRequest**](CheckpointFirewallDataSourceRequest.md)| Add a vSec Checkpoint firewall as data source | 

### Return type

[**CheckpointFirewallDataSource**](CheckpointFirewallDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddCiscoSwitch**
> CiscoSwitchDataSource AddCiscoSwitch(ctx, optional)
Create a cisco switch data source

Add cisco switch as data source. User must provide one of ip or fqdn field in the request body. Appropriate proxy id is retrieved from infra/nodes URL to select the proxy node.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddCiscoSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddCiscoSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of CiscoSwitchDataSourceRequest**](CiscoSwitchDataSourceRequest.md)| Add a cisco switch as datasource. | 

### Return type

[**CiscoSwitchDataSource**](CiscoSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddDellSwitch**
> DellSwitchDataSource AddDellSwitch(ctx, optional)
Create a dell switch data source

Add a dell switch as data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddDellSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddDellSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of DellSwitchDataSourceRequest**](DellSwitchDataSourceRequest.md)|  | 

### Return type

[**DellSwitchDataSource**](DellSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddHpovManager**
> HpOneViewManagerDataSource AddHpovManager(ctx, optional)
Create a hp oneview manager data source

Add a hp oneview manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddHpovManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddHpovManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of HpOneViewManagerDataSourceRequest**](HpOneViewManagerDataSourceRequest.md)|  | 

### Return type

[**HpOneViewManagerDataSource**](HPOneViewManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddHpvcManager**
> HpvcManagerDataSource AddHpvcManager(ctx, optional)
Create a hpvc manager data source

Add hpvc manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddHpvcManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddHpvcManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of HpvcManagerDataSourceRequest**](HpvcManagerDataSourceRequest.md)| Add a switch as datasource | 

### Return type

[**HpvcManagerDataSource**](HPVCManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddJuniperSwitch**
> JuniperSwitchDataSource AddJuniperSwitch(ctx, optional)
Add a juniper switch as data source

Add switch Datasource

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddJuniperSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddJuniperSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of JuniperSwitchDataSourceRequest**](JuniperSwitchDataSourceRequest.md)| Add a cisco switch as datasource | 

### Return type

[**JuniperSwitchDataSource**](JuniperSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddNsxtManagerDatasource**
> NsxtManagerDataSource AddNsxtManagerDatasource(ctx, optional)
Create an nsx-t manager data source

Add an nsx-t manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddNsxtManagerDatasourceOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddNsxtManagerDatasourceOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of NsxtManagerDataSourceRequest**](NsxtManagerDataSourceRequest.md)|  | 

### Return type

[**NsxtManagerDataSource**](NSXTManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddNsxvManagerDatasource**
> NsxvManagerDataSource AddNsxvManagerDatasource(ctx, optional)
Create an nsx-v manager data source

Add an nsx-v manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddNsxvManagerDatasourceOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddNsxvManagerDatasourceOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of NsxvManagerDataSourceRequest**](NsxvManagerDataSourceRequest.md)|  | 

### Return type

[**NsxvManagerDataSource**](NSXVManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddPanoramaFirewall**
> PanFirewallDataSource AddPanoramaFirewall(ctx, optional)
Create panorama firewall data source

Add panorama firewall as data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddPanoramaFirewallOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddPanoramaFirewallOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of PanFirewallDataSourceRequest**](PanFirewallDataSourceRequest.md)| Add a panorama firewall as datasource | 

### Return type

[**PanFirewallDataSource**](PanFirewallDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddUcsManager**
> UcsManagerDataSource AddUcsManager(ctx, optional)
Create a ucs manager data source

Add a ucs manager as data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AddUcsManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a AddUcsManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of UcsManagerDataSourceRequest**](UcsManagerDataSourceRequest.md)|  | 

### Return type

[**UcsManagerDataSource**](UCSManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddVcenterDatasource**
> VCenterDataSource AddVcenterDatasource(ctx, body)
Create a vCenter data source

Add a vcenter data source. User must provide one of ip or fqdn field in the request body. Appropriate proxy id is retrieved from infra/nodes URL to select the proxy node.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**VCenterDataSourceRequest**](VCenterDataSourceRequest.md)| VCenter Credentials | 

### Return type

[**VCenterDataSource**](VCenterDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteAristaSwitch**
> DeleteAristaSwitch(ctx, id)
Delete an arista switch data source

Delete an arista switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteBrocadeSwitch**
> DeleteBrocadeSwitch(ctx, id)
Delete a brocade switch data source

Delete a brocade switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteCheckpointFirewall**
> DeleteCheckpointFirewall(ctx, id)
Delete a checkpoint firewall data source

Delete a checkpoint firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteCiscoSwitch**
> DeleteCiscoSwitch(ctx, id)
Delete a cisco switch data source

Delete a cisco switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteDellSwitch**
> DeleteDellSwitch(ctx, id)
Delete a dell switch data source

Delete a data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteHpovManager**
> DeleteHpovManager(ctx, id)
Delete a hp oneview data source

Delete a hp oneview data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteHpvcManager**
> DeleteHpvcManager(ctx, id)
Delete a hpvc manager data source

Delete a hpvc manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteJuniperSwitch**
> DeleteJuniperSwitch(ctx, id)
Delete a juniper switch data source

Delete a juniper switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteNsxtManager**
> DeleteNsxtManager(ctx, id)
Delete an nsx-t manager data source

Delete an nsx-t manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteNsxvManager**
> DeleteNsxvManager(ctx, id)
Delete an nsx-v manager data source

Delete an nsx-v manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeletePanoramaFirewall**
> DeletePanoramaFirewall(ctx, id)
Delete a panorama firewall data source

Delete a panorama firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteUcsManager**
> DeleteUcsManager(ctx, id)
Delete a ucs manager data source

Delete a ucs manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteVcenter**
> DeleteVcenter(ctx, id)
Delete a vCenter data source

Delete a data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableAristaSwitch**
> DisableAristaSwitch(ctx, id)
Disable an arista switch data source

Disable an arista switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableBrocadeSwitch**
> DisableBrocadeSwitch(ctx, id)
Disable a brocade switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableCheckpointFirewall**
> DisableCheckpointFirewall(ctx, id)
Disable a checkpoint firewall data source

Disable a checkpoint firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableCiscoSwitch**
> DisableCiscoSwitch(ctx, id)
Disable a cisco switch data source

Disable a cisco switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableDellSwitch**
> DisableDellSwitch(ctx, id)
Disable a dell switch data source

Disable a dell switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableHpovManager**
> DisableHpovManager(ctx, id)
Disable a hp oneview data source

Disable a hp oneview data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableHpvcManager**
> DisableHpvcManager(ctx, id)
Disable a hpvc manager data source

Disable a hpvc manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableJuniperSwitch**
> DisableJuniperSwitch(ctx, id)
Disable a juniper switch data source

Disable a juniper switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableNsxtManager**
> DisableNsxtManager(ctx, id)
Disable an nsx-t manager data source

Disable an nsx-t manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableNsxvManager**
> DisableNsxvManager(ctx, id)
Disable an nsx-v manager data source

Disable an nsx-v manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisablePanoramaFirewall**
> DisablePanoramaFirewall(ctx, id)
Disable a panorama firewall data source

Disable a panorama firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableUcsManager**
> DisableUcsManager(ctx, id)
Disable a ucs manager data source

Disable a ucs manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisableVcenter**
> DisableVcenter(ctx, id)
Disable a vCenter data source

Disable a vCenter data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableAristaSwitch**
> EnableAristaSwitch(ctx, id)
Enable an arista switch data source

Enable an arista switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableBrocadeSwitch**
> EnableBrocadeSwitch(ctx, id)
Enable a brocade switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableCheckpointFirewall**
> EnableCheckpointFirewall(ctx, id)
Enable a checkpoint firewall data source

Enable a checkpoint firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableCiscoSwitch**
> EnableCiscoSwitch(ctx, id)
Enable a cisco switch data source

Enable a cisco switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableDellSwitch**
> EnableDellSwitch(ctx, id)
Enable a dell switch data source

Enable a dell switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableHpovManager**
> EnableHpovManager(ctx, id)
Enable a hp oneview data source

Enable a hp oneview data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableHpvcManager**
> EnableHpvcManager(ctx, id)
Enable a hpvc manager data source

Enable a hpvc manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableJuniperSwitch**
> EnableJuniperSwitch(ctx, id)
Enable a juniper switch data source

Enable a juniper switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableNsxtManager**
> EnableNsxtManager(ctx, id)
Enable an nsx-t manager data source

Enable an nsx-t manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableNsxvManager**
> EnableNsxvManager(ctx, id)
Enable an nsx-v manager data source

Enable an nsx-v manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnablePanoramaFirewall**
> EnablePanoramaFirewall(ctx, id)
Enable a panorama firewall data source

Enable a panorama firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableUcsManager**
> EnableUcsManager(ctx, id)
Enable a ucs manager data source

Enable a ucs manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnableVcenter**
> EnableVcenter(ctx, id)
Enable a vCenter data source

Enable a vCenter data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

 (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAristaSwitch**
> AristaSwitchDataSource GetAristaSwitch(ctx, id)
Show arista switch data source details

Show arista switch data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**AristaSwitchDataSource**](AristaSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAristaSwitchSnmpConfig**
> SnmpConfig GetAristaSwitchSnmpConfig(ctx, id)
Show snmp config for arista switch data source

Show snmp config for arista switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetBrocadeSwitch**
> BrocadeSwitchDataSource GetBrocadeSwitch(ctx, id)
Show brocade switch data source details

Show brocade switch data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**BrocadeSwitchDataSource**](BrocadeSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetBrocadeSwitchSnmpConfig**
> SnmpConfig GetBrocadeSwitchSnmpConfig(ctx, id)
Show snmp config for brocade switch data source

Show snmp config for brocade switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetCheckpointFirewall**
> CheckpointFirewallDataSource GetCheckpointFirewall(ctx, id)
Show checkpoint firewall data source details

Show checkpoint firewall data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**CheckpointFirewallDataSource**](CheckpointFirewallDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetCiscoSwitch**
> CiscoSwitchDataSource GetCiscoSwitch(ctx, id)
Show cisco switch data source details

Show cisco switch data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**CiscoSwitchDataSource**](CiscoSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetCiscoSwitchSnmpConfig**
> SnmpConfig GetCiscoSwitchSnmpConfig(ctx, id)
Show snmp config for cisco switch data source

Show snmp config for cisco switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetDellSwitch**
> DellSwitchDataSource GetDellSwitch(ctx, id)
Show dell switch data source details

Get a dell switch data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**DellSwitchDataSource**](DellSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetDellSwitchSnmpConfig**
> SnmpConfig GetDellSwitchSnmpConfig(ctx, id)
Show snmp config for dell switch data source

Show snmp config for dell switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetHpovManager**
> HpOneViewManagerDataSource GetHpovManager(ctx, id)
Show hp oneview data source details

Show hp oneview data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**HpOneViewManagerDataSource**](HPOneViewManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetHpvcManager**
> HpvcManagerDataSource GetHpvcManager(ctx, id)
Show hpvc data source details

Show hpvc data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**HpvcManagerDataSource**](HPVCManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetJuniperSwitch**
> JuniperSwitchDataSource GetJuniperSwitch(ctx, id)
Show juniper switch data source details

Show juniper switch data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**JuniperSwitchDataSource**](JuniperSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetJuniperSwitchSnmpConfig**
> SnmpConfig GetJuniperSwitchSnmpConfig(ctx, id)
Show snmp config for juniper switch data source

Show snmp config for juniper switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetNsxtManager**
> NsxtManagerDataSource GetNsxtManager(ctx, id)
Show nsx-t manager data source details

Show nsx-t manager data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**NsxtManagerDataSource**](NSXTManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetNsxvControllerCluster**
> NsxControllerDataCollection GetNsxvControllerCluster(ctx, id)
Show nsx controller-cluster details

Show nsx controller-cluster details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**NsxControllerDataCollection**](NSXControllerDataCollection.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetNsxvManager**
> NsxvManagerDataSource GetNsxvManager(ctx, id)
Show nsx-v manager data source details

Show nsx-v manager data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**NsxvManagerDataSource**](NSXVManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetPanoramaFirewall**
> PanFirewallDataSource GetPanoramaFirewall(ctx, id)
Show panorama firewall data source details

Show panorama firewall data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**PanFirewallDataSource**](PanFirewallDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetUcsManager**
> UcsManagerDataSource GetUcsManager(ctx, id)
Show ucs manager data source details

Show ucs manager data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**UcsManagerDataSource**](UCSManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetUcsSnmpConfig**
> SnmpConfig GetUcsSnmpConfig(ctx, id)
Show snmp config for ucs fabric interconnects

Show snmp config for ucs fabric interconnects

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVcenter**
> VCenterDataSource GetVcenter(ctx, id)
Show vCenter data source details

Show vCenter data source details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 

### Return type

[**VCenterDataSource**](VCenterDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListAristaSwitches**
> DataSourceListResponse ListAristaSwitches(ctx, )
List arista switch data sources

List arista switch data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListBrocadeSwitches**
> DataSourceListResponse ListBrocadeSwitches(ctx, )
List brocade switch data sources

List brocade switch data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListCheckpointFirewalls**
> DataSourceListResponse ListCheckpointFirewalls(ctx, )
List checkpoint firewall data sources

List checkpoint firewall data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListCiscoSwitches**
> DataSourceListResponse ListCiscoSwitches(ctx, )
List cisco switch data sources

List cisco switch data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListDellSwitches**
> DataSourceListResponse ListDellSwitches(ctx, )
List dell switch data sources

List dell switch data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListHpovManagers**
> DataSourceListResponse ListHpovManagers(ctx, )
List hp oneview manager data sources

List hp oneview manager data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListHpvcManagers**
> DataSourceListResponse ListHpvcManagers(ctx, )
List hpvc manager data sources

List hpvc manager data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListJuniperSwitches**
> DataSourceListResponse ListJuniperSwitches(ctx, )
List juniper switch data sources

List juniper switch data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListNsxtManagers**
> DataSourceListResponse ListNsxtManagers(ctx, )
List nsx-t manager data sources

List nsx-t manager data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListNsxvManagers**
> DataSourceListResponse ListNsxvManagers(ctx, )
List nsx-v manager data sources

List nsx-v manager data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListPanoramaFirewalls**
> DataSourceListResponse ListPanoramaFirewalls(ctx, )
List panorama firewall data sources

List panorama firewall data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListUcsManagers**
> DataSourceListResponse ListUcsManagers(ctx, )
List ucs manager data sources

List ucs manager data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListVcenters**
> DataSourceListResponse ListVcenters(ctx, )
List vCenter data sources

List vCenter data sources

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**DataSourceListResponse**](DataSourceListResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateAristaSwitch**
> AristaSwitchDataSource UpdateAristaSwitch(ctx, id, optional)
Update an arista switch data source

Update an switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateAristaSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateAristaSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of AristaSwitchDataSource**](AristaSwitchDataSource.md)|  | 

### Return type

[**AristaSwitchDataSource**](AristaSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateAristaSwitchSnmpConfig**
> SnmpConfig UpdateAristaSwitchSnmpConfig(ctx, id, optional)
Update snmp config for arista switch data source

Update snmp config for arista switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateAristaSwitchSnmpConfigOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateAristaSwitchSnmpConfigOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of SnmpConfig**](SnmpConfig.md)|  | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateBrocadeSwitch**
> BrocadeSwitchDataSource UpdateBrocadeSwitch(ctx, id, optional)
Update a brocade switch data source

Update a brocade switch data source. Only credentials, nickname and notes can be updated.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateBrocadeSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateBrocadeSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of BrocadeSwitchDataSource**](BrocadeSwitchDataSource.md)|  | 

### Return type

[**BrocadeSwitchDataSource**](BrocadeSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateBrocadeSwitchSnmpConfig**
> SnmpConfig UpdateBrocadeSwitchSnmpConfig(ctx, id, optional)
Update snmp config for brocade switch data source

Update snmp config for brocade switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateBrocadeSwitchSnmpConfigOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateBrocadeSwitchSnmpConfigOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of SnmpConfig**](SnmpConfig.md)|  | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateCheckpointFirewall**
> CheckpointFirewallDataSource UpdateCheckpointFirewall(ctx, id, optional)
Update a checkpoint firewall data source

Update a checkpoint firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateCheckpointFirewallOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateCheckpointFirewallOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of CheckpointFirewallDataSource**](CheckpointFirewallDataSource.md)|  | 

### Return type

[**CheckpointFirewallDataSource**](CheckpointFirewallDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateCiscoSwitch**
> CiscoSwitchDataSource UpdateCiscoSwitch(ctx, id, optional)
Update a cisco switch data source

Update a cisco switch data source. Only credentials, nickname and notes can be updated.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateCiscoSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateCiscoSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of CiscoSwitchDataSource**](CiscoSwitchDataSource.md)|  | 

### Return type

[**CiscoSwitchDataSource**](CiscoSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateCiscoSwitchSnmpConfig**
> SnmpConfig UpdateCiscoSwitchSnmpConfig(ctx, id, optional)
Update snmp config for cisco switch data source

Update snmp config for cisco switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateCiscoSwitchSnmpConfigOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateCiscoSwitchSnmpConfigOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of SnmpConfig**](SnmpConfig.md)|  | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateDellSwitch**
> DellSwitchDataSource UpdateDellSwitch(ctx, id, optional)
Update a dell switch data source

Update a dell switch data source. Only credentials, nickname and notes can be updated

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateDellSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateDellSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of DellSwitchDataSource**](DellSwitchDataSource.md)|  | 

### Return type

[**DellSwitchDataSource**](DellSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateDellSwitchSnmpConfig**
> SnmpConfig UpdateDellSwitchSnmpConfig(ctx, id, optional)
Update snmp config for dell switch data source

Update snmp config for dell switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateDellSwitchSnmpConfigOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateDellSwitchSnmpConfigOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of SnmpConfig**](SnmpConfig.md)|  | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateHpovManager**
> HpOneViewManagerDataSource UpdateHpovManager(ctx, id, optional)
Update a hp oneview data source

Update a hp oneview data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateHpovManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateHpovManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of HpOneViewManagerDataSource**](HpOneViewManagerDataSource.md)|  | 

### Return type

[**HpOneViewManagerDataSource**](HPOneViewManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateHpvcManager**
> HpvcManagerDataSource UpdateHpvcManager(ctx, id, optional)
Update a hpvc manager data source

Update a hpvc manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateHpvcManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateHpvcManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of HpvcManagerDataSource**](HpvcManagerDataSource.md)|  | 

### Return type

[**HpvcManagerDataSource**](HPVCManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateJuniperSwitch**
> JuniperSwitchDataSource UpdateJuniperSwitch(ctx, id, optional)
Update a juniper switch data source

Update a juniper switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateJuniperSwitchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateJuniperSwitchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of JuniperSwitchDataSource**](JuniperSwitchDataSource.md)|  | 

### Return type

[**JuniperSwitchDataSource**](JuniperSwitchDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateJuniperSwitchSnmpConfig**
> SnmpConfig UpdateJuniperSwitchSnmpConfig(ctx, id, optional)
Update snmp config for a juniper switch data source

Update snmp config for a juniper switch data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateJuniperSwitchSnmpConfigOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateJuniperSwitchSnmpConfigOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of SnmpConfig**](SnmpConfig.md)|  | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateNsxtManager**
> NsxtManagerDataSource UpdateNsxtManager(ctx, id, optional)
Update an nsx-t manager data source

Update an nsx-t manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateNsxtManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateNsxtManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of NsxtManagerDataSource**](NsxtManagerDataSource.md)|  | 

### Return type

[**NsxtManagerDataSource**](NSXTManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateNsxvControllerCluster**
> NsxControllerDataCollection UpdateNsxvControllerCluster(ctx, id, optional)
Update nsx controller-cluster details

Update nsx controller-cluster details

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateNsxvControllerClusterOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateNsxvControllerClusterOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of NsxControllerDataCollection**](NsxControllerDataCollection.md)|  | 

### Return type

[**NsxControllerDataCollection**](NSXControllerDataCollection.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateNsxvManager**
> NsxvManagerDataSource UpdateNsxvManager(ctx, id, optional)
Update an nsx-v manager data source

Update an nsx-v manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateNsxvManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateNsxvManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of NsxvManagerDataSource**](NsxvManagerDataSource.md)|  | 

### Return type

[**NsxvManagerDataSource**](NSXVManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdatePanoramaFirewall**
> PanFirewallDataSource UpdatePanoramaFirewall(ctx, id, optional)
Update a panorama firewall data source

Update a panorama firewall data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdatePanoramaFirewallOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdatePanoramaFirewallOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of PanFirewallDataSource**](PanFirewallDataSource.md)|  | 

### Return type

[**PanFirewallDataSource**](PanFirewallDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateUcsManager**
> UcsManagerDataSource UpdateUcsManager(ctx, id, optional)
Update a ucs manager data source

Update a ucs manager data source

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateUcsManagerOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateUcsManagerOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of UcsManagerDataSource**](UcsManagerDataSource.md)|  | 

### Return type

[**UcsManagerDataSource**](UCSManagerDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateUcsSnmpConfig**
> SnmpConfig UpdateUcsSnmpConfig(ctx, id, optional)
Update snmp config for ucs fabric interconnects

Update snmp config for ucs fabric interconnects

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateUcsSnmpConfigOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateUcsSnmpConfigOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of SnmpConfig**](SnmpConfig.md)|  | 

### Return type

[**SnmpConfig**](SNMPConfig.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UpdateVcenter**
> VCenterDataSource UpdateVcenter(ctx, id, optional)
Update a vCenter data source.

Update a vcenter data source. Only nickname, notes and credentials can be updated.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| entity id | 
 **optional** | ***UpdateVcenterOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a UpdateVcenterOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of VCenterDataSource**](VCenterDataSource.md)|  | 

### Return type

[**VCenterDataSource**](VCenterDataSource.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

