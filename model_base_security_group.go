/*
 * vRealize Network Insight API Reference
 *
 * vRealize Network Insight API Reference
 *
 * API version: 1.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type BaseSecurityGroup struct {
	EntityId string `json:"entity_id,omitempty"`
	Name string `json:"name,omitempty"`
	EntityType *EntityType `json:"entity_type,omitempty"`
	Members []Reference `json:"members,omitempty"`
	DirectSourceRules []RuleSet `json:"direct_source_rules,omitempty"`
	DirectDestinationRules []RuleSet `json:"direct_destination_rules,omitempty"`
	IndirectSourceRules []RuleSet `json:"indirect_source_rules,omitempty"`
	IndirectDestinationRules []RuleSet `json:"indirect_destination_rules,omitempty"`
	Parents []Reference `json:"parents,omitempty"`
	DirectMembers []Reference `json:"direct_members,omitempty"`
	VendorId string `json:"vendor_id,omitempty"`
	ExcludedMembers []Reference `json:"excluded_members,omitempty"`
}
