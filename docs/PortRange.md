# PortRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Start** | **int32** |  | [optional] [default to null]
**End** | **int32** |  | [optional] [default to null]
**Display** | **string** |  | [optional] [default to null]
**IanaName** | **string** |  | [optional] [default to null]
**IanaPortDisplay** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


