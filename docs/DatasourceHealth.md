# DatasourceHealth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HealthStatus** | **string** |  | [optional] [default to null]
**HealthMessage** | **string** |  | [optional] [default to null]
**HealthErrorCode** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


