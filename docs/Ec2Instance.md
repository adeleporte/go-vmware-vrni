# Ec2Instance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**IpAddresses** | [**[]IpV4Address**](IpV4Address.md) |  | [optional] [default to null]
**DefaultGateway** | **string** |  | [optional] [default to null]
**Vnics** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SecurityGroups** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**SourceFirewallRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**DestinationFirewallRules** | [**[]RuleSet**](RuleSet.md) |  | [optional] [default to null]
**IpSets** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Vpc** | [***Reference**](Reference.md) |  | [optional] [default to null]
**Region** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


