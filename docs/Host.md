# Host

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**EntityType** | [***EntityType**](EntityType.md) |  | [optional] [default to null]
**Vmknics** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**Cluster** | [***Reference**](Reference.md) |  | [optional] [default to null]
**VcenterManager** | [***Reference**](Reference.md) |  | [optional] [default to null]
**VmCount** | **int32** |  | [optional] [default to null]
**Datastores** | [**[]Reference**](Reference.md) |  | [optional] [default to null]
**ServiceTag** | **string** |  | [optional] [default to null]
**VendorId** | **string** |  | [optional] [default to null]
**NsxManager** | [***Reference**](Reference.md) |  | [optional] [default to null]
**MaintenanceMode** | **string** |  | [optional] [default to null]
**ConnectionState** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


